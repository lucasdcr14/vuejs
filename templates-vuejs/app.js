new Vue({
    el: '#app',
    data: {
        title: 'Hello World!',
        link: 'http://google.com',
        finishedLink: '<a href="https://google.com">Google</a>'
    },
    methods: {
        sayHello: function() {
            //return 'Hello!'
            this.title = 'Hello!';
            return this.title;
        }
    }
});