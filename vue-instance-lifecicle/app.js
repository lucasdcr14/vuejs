new Vue({
    el: '#app',
    data: {
        title: 'The VueJS Instance'
    }, 
    method: {

    },
    beforeCreate: function() {
        console.log('beforeCreate()');
    },
    created: function() {
        console.log('created()');
    },
    beforeMount: function() {
        console.log('beforeMount()');
    },
    mounted: function() {
        console.log('mounted()');
    },
    beforeUpdate: function() {
        console.log('beforeUpdate()');
    },
    updated: function() {
        console.log('updated()');
    },
    beforeDestroy: function() {
        console.log('beforeDestroy()');
    },
    destroy: function() {
        console.log('destroy()');
    },
    destroyed: function() {
        console.log("destroyed");
    }
});
